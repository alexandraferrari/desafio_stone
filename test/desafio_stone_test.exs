defmodule DesafioStoneTest do
  use ExUnit.Case
  doctest DesafioStone

  test "divide two items between three emails" do
    items = [
      %Item{ name: "Jarra", quantity: 2, price_unit: 3000 },
      %Item{ name: "Caneca", quantity: 3, price_unit: 500 },
    ]
    emails = ["alexandra@email.com", "gustavo@email.com"]

    result = %{
      "alexandra@email.com" => 3750,
      "gustavo@email.com" => 3750
    }

    assert DesafioStone.calculate(items, emails) == result
  end

  test "divide an item between three emails" do
    items = [
      %Item{ name: "Fone de ouvido", quantity: 1, price_unit: 100 }
    ]

    emails = ["alexandra@email.com", "teste@email.com", "maria@email.com"]

    result = %{
      "alexandra@email.com" => 34,
      "teste@email.com" => 33,
      "maria@email.com" => 33
    }

    assert DesafioStone.calculate(items, emails) == result
  end

  test "divide an item between three emails and distribute" do
    items = [
      %Item{ name: "Fone de ouvido", quantity: 1, price_unit: 101 }
    ]

    emails = ["alexandra@email.com", "teste@email.com", "maria@email.com"]

    result = %{
      "alexandra@email.com" => 34,
      "teste@email.com" => 34,
      "maria@email.com" => 33
    }

    assert DesafioStone.calculate(items, emails) == result
  end

  test "divide an item between zero emails" do
    items = [
      %Item{ name: "Fone de ouvido", quantity: 1, price_unit: 101 }
    ]

    emails = []

    result = %{}

    assert DesafioStone.calculate(items, emails) == result
  end

  test "divide zero item between emails" do
    items = []

    emails = ["alexandra@email.com", "teste@email.com", "maria@email.com"]

    result = %{}

    assert DesafioStone.calculate(items, emails) == result
  end

  test "empty items list and empty emails list" do
    items = []

    emails = ["alexandra@email.com", "teste@email.com", "maria@email.com"]

    result = %{}

    assert DesafioStone.calculate(items, emails) == result
  end


  test "divide an item between three emails without remainder" do
    items = [
      %Item{ name: "Fone de ouvido", quantity: 1, price_unit: 300 }
    ]

    emails = ["alexandra@email.com", "teste@email.com", "maria@email.com"]

    result = %{
      "alexandra@email.com" => 100,
      "teste@email.com" => 100,
      "maria@email.com" => 100
    }

    assert DesafioStone.calculate(items, emails) == result
  end

  test "divide an item between emails" do
    items = [
      %Item{ name: "Fone de ouvido", quantity: 1, price_unit: 2 }
    ]

    emails = ["alexandra@email.com", "teste@email.com", "maria@email.com"]

    result = %{
      "alexandra@email.com" => 1,
      "teste@email.com" => 1,
      "maria@email.com" => 0
    }

    assert DesafioStone.calculate(items, emails) == result
  end
end
