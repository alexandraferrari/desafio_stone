defmodule DesafioStone do
  @spec calculate([Item], [String.t()]) :: %{String.t() => integer()}
  def calculate(_items, []) do
    %{}
  end

  def calculate([], _emails) do
    %{}
  end

  def calculate(items, emails) do
    total = total_items(items)
    calculate_value(total, emails)
  end

  @spec total_item(Item) :: integer()
  defp total_item(item) do
    item.quantity * item.price_unit
  end

  @spec total_items([Item]) :: integer()
  defp total_items(items) do
    items
      |> Enum.map(fn(item) -> total_item(item) end)
      |> Enum.sum()
  end

  @spec calculate_value(integer(), [String.t()]) :: %{String.t() => integer()}
  defp calculate_value(value, emails) do
    quantity_emails = Enum.count(emails)
    remainder = rem(value, quantity_emails)

    {remainders, amounts_per_email} =
      1..quantity_emails
      |> Enum.map(fn(_) -> div(value, quantity_emails) end)
      |> Enum.split(remainder)

    remainders = Enum.map(remainders, fn(value) -> value + 1 end)

    Enum.into(Enum.zip(emails, Enum.concat(remainders, amounts_per_email)), %{})
  end
end

defmodule Item do
  defstruct [:name, :quantity, :price_unit]
end
